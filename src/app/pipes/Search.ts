import { Notes } from './../models/Notes';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'textSearch',
	pure: false
})
export class TextSearchPipe implements PipeTransform {
	transform(items: Notes[], term: string): any {
		if (!items || !term) {
			return items;
		}
		return items.filter(item => item.title.indexOf(term) !== -1);
	}
}
