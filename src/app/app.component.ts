import { Notes } from './models/Notes';
import { Component } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import * as NotesActions from './actions/notes.actions';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';

interface AppState {
	notes: Notes[];
}

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent {
	title = 'app';
	term: string;
	notes: Notes[] = [];
	draweropen: boolean;
	currentNoteIndex = 0;
	constructor(public store: Store<AppState>, breakpointObserver: BreakpointObserver) {
		store.subscribe((data) => {
			breakpointObserver.observe([
				Breakpoints.HandsetPortrait
			]).subscribe(result => {
				this.draweropen = !result.matches;
			});
			data.notes.sort((a: Notes, b: Notes) => {
				!a.updated ? a.updated = a.created : console.log();
				!b.updated ? b.updated = b.created : console.log();
				if (a.updated && b.updated) {
					if (a.updated < b.updated) {
						return 1;
					} else if (a.updated > b.updated) {
						return -1;
					} else {
						return 0;
					}
				}
				return 0;
			});
			this.notes = data.notes;
			this.currentNoteIndex = 0;
		});
	}

	addNew() {
		let note: Notes = {
			title: `New Notes`,
			description: '',
			created: new Date().getTime()
		};
		this.store.dispatch(new NotesActions.AddNotesAction(note));
	}

	updateNote() {
		this.notes[this.currentNoteIndex].title = this.notes[this.currentNoteIndex].description ? this.notes[this.currentNoteIndex].description.split('\n')[0] : 'New Notes';
		this.notes[this.currentNoteIndex].description = this.notes[this.currentNoteIndex].description;
		this.notes[this.currentNoteIndex].updated = new Date().getTime();
		this.store.dispatch(new NotesActions.SaveNotesAction(this.currentNoteIndex, this.notes[this.currentNoteIndex]));
	}

	deleteNote() {
		this.store.dispatch(new NotesActions.DeleteNotesAction(this.currentNoteIndex));
	}

}
