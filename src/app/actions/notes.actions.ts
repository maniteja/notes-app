import { Notes } from './../models/Notes';
import { ActionReducer, Action } from '@ngrx/store';

export enum NotesActionTypes {
	ADD = 'ADD',
	SAVE = 'SAVE',
	DELETE = 'DELETE'
}

export class AddNotesAction implements Action {
	readonly type = NotesActionTypes.ADD;
	constructor(public payload: Notes) {}
}

export class SaveNotesAction implements Action {
	readonly type = NotesActionTypes.SAVE;
	constructor(public index: number, public payload: Notes) {}
}

export class DeleteNotesAction implements Action {
	readonly type = NotesActionTypes.DELETE;
	constructor(public index: number) {}
}

export type NotesActions = AddNotesAction | SaveNotesAction | DeleteNotesAction;
