import { ActionReducer, Action } from '@ngrx/store';

import { NotesActionTypes, NotesActions } from '../actions/notes.actions';

import { Notes } from '../models/Notes';

let data = JSON.parse(localStorage.getItem('notes'));

export function notesReducer(state: Notes[] = data || [], action: NotesActions) {
	switch (action.type) {
		case NotesActionTypes.ADD:
			state.push(action.payload);
			localStorage.setItem('notes', JSON.stringify(state));
			return state;
		case NotesActionTypes.SAVE:
			state[action.index] = action.payload;
			// save to db
			localStorage.setItem('notes', JSON.stringify(state));
			return state;
		case NotesActionTypes.DELETE:
			state.splice(action.index, 1);
			localStorage.setItem('notes', JSON.stringify(state));
			return state;
		default:
			return state;
	}
}
