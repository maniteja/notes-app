import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { MomentModule } from 'angular2-moment';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { MatSidenavModule, MatListModule, MatFormFieldModule, MatToolbarModule, MatInputModule, MatIconModule, MatButtonModule } from '@angular/material';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';

import { notesReducer } from './reducers/notes.reducer';
import { TextSearchPipe } from './pipes/Search';
import { LayoutModule } from '@angular/cdk/layout';


@NgModule({
	declarations: [
		AppComponent,
		HomeComponent,
		TextSearchPipe
	],
	imports: [
		BrowserModule,
		FormsModule,
		MomentModule,
		MatSidenavModule,
		MatListModule,
		MatFormFieldModule,
		MatInputModule,
		MatToolbarModule,
		MatIconModule,
		MatButtonModule,
		LayoutModule,
		BrowserAnimationsModule,
		StoreModule.forRoot({ notes: notesReducer}),
		FroalaEditorModule.forRoot(),
		FroalaViewModule.forRoot()
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
