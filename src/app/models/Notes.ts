export interface Notes {
	title: string;
	description?: string;
	created: number;
	updated?: number;
}
